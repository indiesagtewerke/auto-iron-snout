# Auto Iron Snout

Auto press through the game Iron Snout. This auto fighter script will repeatedly press left and right to fight wolfs. This is for the *10000 Total Kills* achievement.

## Requirements

- Windows operating system
- [AutoIt v3](https://www.autoitscript.com/site/) installed

## How to use

1. Run this auto press script `"C:\Program Files (x86)\AutoIt3\AutoIt3.exe" Auto-Iron-Snout-Fighter.au3`.
2. Open 'Iron Snout' in full screen from Steam, and start a level for the script to play.
3. Close the dailog that confirms that the script started, the script will start pressing buttons.
4. Press the 'Esc' key to stop the script's execution earlier than the specified amount of plays in the script.

## Achievements

To get the most out of this script, play on the forest level. This will get you at least these achievements:

- Save the Forest
- Many Punch Pig
- 5000 Total Kills
- 10000 Total Kills

## Tools used for Development

- Click automation developed with the [AutoIt3][autoit] scripting language.

[autoit]: https://www.autoitscript.com/site/
