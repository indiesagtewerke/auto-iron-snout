#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.14.5
 Author:         Quintin Henn

 Script Function:
	Automate the game Iron Snout.

#ce ----------------------------------------------------------------------------

#include <AutoItConstants.au3>
#include <MsgBoxConstants.au3>
#include <Misc.au3>

Const $appName = "Auto Iron Snout Fighter"

Global $escapeKeyCode = "1B"
Global $hDLL = DllOpen("user32.dll")

Const $playTime = 59999
Local $escPressed = False

MsgBox($MB_ICONINFORMATION, $appName, "Starting. Waiting for Iron Snout to become active...")

Local $hWnd = WinWaitActive("[TITLE:Iron Snout; CLASS:YYGameMakerYY;]")

Sleep(500)

For $playCount = 0 To $playTime Step 1

  Send("{ENTER}")
  Send("{LEFT}")
  Send("{RIGHT}")

  If _IsPressed($escapeKeyCode, $hDLL) Then
    MsgBox($MB_SYSTEMMODAL, $appName, "The Esc Key was pressed, therefore we will close the application.")
    ExitLoop
  EndIf

Next

MsgBox($MB_SYSTEMMODAL, $appName, "You have played " & $playCount & " times!")
